package fx_timer;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;

/**
 *
 * @author JuanP
 */
public class TimeHanndler extends Thread
{
    public double currentTime,startTime,timeFrom;
    public boolean stop;
    private InterfaceController theInterface;
    private ArrayList<Double> clics;
    
    public TimeHanndler(InterfaceController theInterface)
    {
        this.theInterface = theInterface;
        reset();
    }
    
    public void reset()
    {
        clics = new ArrayList<>();
        currentTime = 0;
        timeFrom = startTime = System.currentTimeMillis();
        stop = false;
    }
    
    private double getCurrentTime()
    {
        return System.currentTimeMillis() - startTime;
    }
    
    public void action()
    {
        timeFrom = System.currentTimeMillis();
        clics.add(currentTime);
    }
        
    public int getLastClics() { return clics.size(); }
   
    private static double round(double value, int places)
    {
        if (places < 0)
        {
            return 0;
        }
        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    
    private double getTimeDifference(double time)
    {
        return this.round((System.currentTimeMillis()-time)/1000, 1);
    }
    
    @Override public void run()
    {
        while(!stop)
        {
            //System.out.println("Time = "+getCurrentTime());
            Platform.runLater(new Runnable()
            {
                @Override
                public void run()
                {
                    currentTime = System.currentTimeMillis();
                    double pastTime = currentTime - 1000 * 2; // last 2 seconds
                    for (Iterator<Double> iterator = clics.iterator(); iterator.hasNext();)
                    {
                        double value = iterator.next();
                        if (value < pastTime)
                        {
                            iterator.remove();
                        }
                    }
                    theInterface.setInterfaceData(getTimeDifference(startTime)+"",
                            getTimeDifference(timeFrom)+"");                    
                }
            });
            try
            {
                Thread.sleep(100);
            } catch (InterruptedException ex)
            {
                Logger.getLogger(TimeHanndler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("Thread finished normally.");
    } 
}
